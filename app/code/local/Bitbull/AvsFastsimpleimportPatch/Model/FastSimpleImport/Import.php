<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10/11/15
 * Time: 18.04
 */ 
class Bitbull_AvsFastsimpleimportPatch_Model_FastSimpleImport_Import extends AvS_FastSimpleImport_Model_Import {
    /**
      * Indica se preservare i links anche
      * in caso di behaviour REPLACE.
      *
      * @var boolean
      */
     protected $_preserveLinks = false;

     /**
      * @return boolean
      */
     public function isPreserveLinks()
     {
         return $this->_preserveLinks;
     }

     /**
      * @param boolean $preserveLinks
      * @return Paniate_Import_Model_FastSimpleImport_Import
      */
     public function setPreserveLinks($preserveLinks)
     {
         $this->_preserveLinks = $preserveLinks;

         return $this;
     }

     /**
      * @param Paniate_Import_Model_FastSimpleImport_Import_Entity_Product $entityAdapter
      *
      * @return void
      */
     public function setEntityAdapter($entityAdapter)
     {
         if ($this->isPreserveLinks()) {
             $entityAdapter->setPreserveLinks(true);
         }

         $this->_entityAdapter = $entityAdapter;
     }


}